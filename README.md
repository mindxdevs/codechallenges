# Code Challenges

## Overview

This is a repository of code challenges for use during Mind-X developer interviews.

## File Format

Each code challenge should be annotated with the following header:


> [challenge name]  
> \-----------------------  
> **Language:** [language the challenge is meant to be completed in]  
> **Subject:** [general topic the challenge covers]  
> **Author:** [author name]  
> **Expected time:** [range of time the average candidate should need to complete the challenge]  
> **Key Skills:** [list of skill keywords that the challenge tests]

> Instructions  
> \-------------------  
> [instructions to provide to candidate]

Multiple challenges can be placed in the same file, with each one under its own header. 
Ideally, each challenge should be paired with at least one example solution. Each example solution should be valid code and should be accompanied by a test case.

## Directory Structure

The repository is primarily divided into two sections:

### Brief

* These are coding questions that should take a candidate 5-30 minutes to solve, and are appropriate to ask during a phone interview.

### Long-form

* These are coding questions that should take a candidate between 30-60 minutes to solve, and are appropriate for in-person interviews.

Each folder is subdivided into folders by language, e.g. C/C++, Python, etc.