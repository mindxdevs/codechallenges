/**
 * C brief code challenge 1
 * -------------------------
 * Language: C
 * Subject: string manipulation
 * Author: Dexter Duckworth
 * Expected time: 10-20 minutes
 * Key Skills: c, pointers, arrays, iteration
 * 
 * 
 * Instructions
 * -------------
 * Implement a function 'void reverse(char *in_str)' that takes a c string as input
 * and reverses the string in place.
 */


//EXAMPLE SOLUTION

#include <string.h>

void reverse(char *in_str) {

    size_t in_len = strlen(in_str);
    int i,j;
    for (i=0,j=in_len-1; i != j; i++,j--) {
        printf("%c, %c\n", in_str[i], in_str[j]);
        char tmp;
        tmp = in_str[i];
        in_str[i] = in_str[j];
        in_str[j] = tmp;
    }
}

//TEST CASE

#include <stdio.h>
#include <assert.h>

int main(int argc, char const *argv[])
{
    char test_string[] = "This is a test string";
    char reversed_string[] = "gnirts tset a si sihT";
    
    reverse(&test_string);
    printf("%s\n", test_string);
    assert(test_string == reversed_string);
    return 0;
}