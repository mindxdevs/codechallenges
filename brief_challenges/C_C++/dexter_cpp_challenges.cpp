/**
 * C++ brief code challenge 1
 * ---------------------------
 * Language: C++ 11
 * Subject: string manipulation
 * Author: Dexter Duckworth
 * Expected time: 10-20 minutes
 * Key Skills: c++, stl, iteration
 * 
 * 
 * Instructions
 * -------------
 * Implement a function 'void reverse(std::string &in_str)' that takes a reference
 * to an std::string as input and reverses the string in place.
 */


//EXAMPLE SOLUTION

#include <string>
#include <utility>

void reverse(std::string &in_str) {
    std::string::iterator fwd = in_str.begin();
    std::string::reverse_iterator bwd = in_str.rbegin();
    for (; fwd != bwd.base() - 1; ++fwd, ++bwd) {
        std::swap(*fwd, *bwd);
    }
}

//TEST CASE

#include <cstdio>
#include <assert.h>

int main(int argc, char const *argv[])
{
    std::string test_string = "This is a test string";
    std::string reversed_string = "gnirts tset a si sihT";
    
    reverse(test_string);
    printf("%s\n", test_string.c_str());

    assert(test_string == reversed_string);
    return 0;
}