#!/usr/bin/env python3

"""
Python brief code challenge 1
------------------------------
Language: Python 3
Subject: string manipulation
Author: Dexter Duckworth
Expected time: 5-10 minutes
Key Skills: python, strings


Instructions
-------------
Implement a python function called 'word_reverse' that takes a string
as an argument and returns a new string with each word in the original
string reversed.

Example output
---------------
    word_reverse("This is a test string")
    > 'sihT si a tset gnirts'
"""


# EXAMPLE SOLUTION
def word_reverse(in_string, delim=' '):
    return delim.join(token[::-1] for token in in_string.split(delim))



"""
Python brief code challenge 2
------------------------------
Language: Python 3
Subject: list filtering
Author: Dexter Duckworth
Expected time: 10-20 minutes
Key Skills: python, iterables, built-in methods


Instructions
-------------
Implement a python function called 'get_longest' that takes a list of
strings as an argument and returns the longest string in the list.

Example output
---------------
    get_longest(["Apple", "Banana", "Pear"])
    > "Banana"

Bonus
------
What is the function's behavior when there are multiple items of the
same length? Create two versions of the function, one that returns the
first matching result, and one that returns a list of all matches.
"""

# EXAMPLE SOLUTION 1: Unique result
def get_longest_unique(words):
    return max(words, key=len)
    

# EXAMPLE SOLUTION 2: All matching results
def get_longest_all(words):
    max_len = len(max(words, key=len))
    return [item for item in words if len(item) == max_len]



# TEST CASES

if __name__ == "__main__":
    # TEST CASE 1
    assert word_reverse('This is a test string') == 'sihT si a tset gnirts'

    # TEST CASE 2
    assert get_longest_unique(["Apple", "Banana", "Pear", "Tomato"]) == "Banana"

    # TEST CASE 2a
    assert get_longest_all(["Apple", "Banana", "Pear", "Tomato"]) == ["Banana", "Tomato"]