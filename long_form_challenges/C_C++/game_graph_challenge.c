/**
 * C/C++ long-form code challenge 1
 * --------------------------------
 * Language: C/C++
 * Subject: graph traversal
 * Author: Dexter Duckworth
 * Expected time: 30-60 minutes
 * Key Skills: c/c++, pointers, recursion, graphs
 */

#include <stdint.h>
#include <stdlib.h>

typedef struct Node Node;

struct Node {
    uint8_t player;
    Node *move_a;
    Node *move_b;
    int payout_1;
    int payout_2;
};

/**
 * Instructions
 * -------------
 * Given the above Node definition, imagine a binary tree constructed of an arbitrary number
 * of nodes. This tree defines a Sequential Two Player Game, where at each internal node, one
 * player chooses option A or B, then play moves to the corresponding child node. This process 
 * repeats until a leaf node is reached, at which point each player receives a payout as defined
 * by the leaf node.
 * 
 * Given the following assumptions, write a function that takes the root node of a tree and
 * returns the maximum expected payout for each player:
 *     * Each player is a rational agent with perfect information: they will always choose
 *       the move that results in the greatest payout, and they have knowledge of all moves
 *       and payouts in the game tree.
 *     * Every internal node has two child nodes, and no payout values.
 *     * Every leaf node has zero child nodes, and has two payout values.
 *     * Players can be assigned moves in any order, and move order is not symmetrical
 *       across the tree, e.g. one branch may have move order P1->P2->P2->P1 while another
 *       may have move order P1->P1->P2->P1.
 *     * Each tree branch may be of any depth.
 * 
 * 
 * Example input
 * --------------
 * 
 *             P1
 *           /    \
 *         P2       P2
 *       /    \    |    \
 *   (2,1)  (1,2) (3,4) (10,1)
 * 
 * Example output
 * ---------------
 * (3, 4)
 * 
 * 
 * Hint
 * -----
 * Algorithm for determining maximally-valued Nash Equilibrium of a sequential game:
 *     * For each pair of leaf nodes, determine the active player corresponding to the
 *       leaf nodes' parent node.
 *     * Select the leaf node from each pair that maximize's the active player's payout,
 *       then set the parent node's payout equal to the selected node's payout. If payouts
 *       are equal for the active player, choose the node that maximizes the payout of the
 *       non-active player.
 *     * Clear the child nodes of the parent node, so that it is now a leaf node.
 *     * Repeat the process for the new leaf nodes' parent nodes, until you reach the root
 *       of the game tree. 
 *     * The root node's payout is now equal to the maximum expected payout for each player.
 */


// EXAMPLE SOLUTION

void get_payout(Node *root, int *payout_1, int *payout_2) {
    if (root->move_a == NULL && root->move_b == NULL) {
        *payout_1 = root->payout_1;
        *payout_2 = root->payout_2;
        return;
    }

    int move_a_payout_1, move_a_payout_2;
    int move_b_payout_1, move_b_payout_2;
    get_payout(root->move_a, &move_a_payout_1, &move_a_payout_2);
    get_payout(root->move_b, &move_b_payout_1, &move_b_payout_2);

    if (root->player == 1 && move_a_payout_1 > move_b_payout_1) {
        *payout_1 = move_a_payout_1;
        *payout_2 = move_a_payout_2;
    } else if (root->player == 1 && move_a_payout_1 < move_b_payout_1) {
        *payout_1 = move_b_payout_1;
        *payout_2 = move_b_payout_2;
    } else if (move_a_payout_2 > move_b_payout_2) {
        *payout_1 = move_a_payout_1;
        *payout_2 = move_a_payout_2;
    } else {
        *payout_1 = move_b_payout_1;
        *payout_2 = move_b_payout_2;
    }

    return;
}


//TEST CASE

#include <stdio.h>
#include <assert.h>

int main(int argc, char const *argv[])
{
    Node root;
    root.player = 1;
    root.move_a = malloc(sizeof(Node));
    root.move_b = malloc(sizeof(Node));

    Node *n1 = root.move_a;
    n1->player = 2;
    n1->move_a = malloc(sizeof(Node));
    n1->move_b = malloc(sizeof(Node));

    Node *n2 = root.move_b;
    n2->player = 2;
    n2->move_a = malloc(sizeof(Node));
    n2->move_b = malloc(sizeof(Node));

    Node *n3 = n1->move_a;
    n3->payout_1 = 2;
    n3->payout_2 = 1;

    Node *n4 = n1->move_b;
    n4->payout_1 = 1;
    n4->payout_2 = 2;

    Node *n5 = n2->move_a;
    n5->payout_1 = 3;
    n5->payout_2 = 4;

    Node *n6 = n2->move_b;
    n6->payout_1 = 10;
    n6->payout_2 = 1;

    int max_payout_1, max_payout_2;
    get_payout(&root, &max_payout_1, &max_payout_2);

    printf("(%i, %i)\n", max_payout_1, max_payout_2);

    assert(max_payout_1 == 3 && max_payout_2 == 4);

    return 0;
}